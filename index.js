console.log("Hello World!");

// [Section] Functions
/*
functions in javascript are lines/blocks of codes that tell our device to perform a certain task when they are called/invoked

functions are mostly created to create complicated tasks to run several lines of code in succession
*/
// Function Declarartion
// defines a function with specified parameters
/*
Syntax:
	function functionName(){
		codes to be executed (statements);
	}
*/
function printName(){
	console.log("Hello Again");
}

printName();

// [Section] Function Invocation
// The code block and statements inside the function is not immediately excecuted when the function is defined
// The codes will be executed when the function is invoked or called

// Let's reuse the codes inside the printName function
printName();

// declaredFunction(); we cannot invoke a function that is not yet defined

// [Section] Function declaration vs expressions
// A function can be created through function declaration through the use of "function" keyword and adding a function name

// In JS, functions can be hoisted. Hoisting is the JS manner wherein functions can be run or used before they are created
function declaredFunction(){
	console.log("Hello from declaredFunction()");
}
declaredFunction();

/*
let and const variables cannot be hoisted in JS
console.log(name);
let name;
*/

// Function Expression
// A function can be stored in a variable
// A function expression is an anonymous function assigned to a variable
/*
Syntax
	let/const variableFunctionName = function(){
		codes to be excecuted/statements
	}
*/
// variableFunction();
/*
	Error - function expressions, being let/const variables
*/
let variableFunction = function(){
	console.log("Hello from variableFunction()");
}

variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the other side!");
}
// funcName();
funcExpression();

// Reassign declared functions and function expression to new anonymous	functions

declaredFunction = function(){
	console.log("Updated declaredFunction");
}
declaredFunction();

funcExpression = function(){
	console.log("Updated funcExpression");
}
funcExpression();

// We cannot reassign a new function expression initialized with const keyword
const constFunction = function(){
	console.log("Const Function");
}
constFunction();

/*constFunction = function(){
	console.log("cannot be reassigned!");
}
constFunction();*/

// [Section] Function Scoping
/*
	
	Scope is the accessibilty (visibilty) of variables

	JS has 3 scopes for its variables
		1) Local/Block scope
		2) Global scope
		3) Function scope
			JavaScript has function scope: Each function creates a new scope


*/

let globalVar = "Jane Doe";
{
	let localVar = "John Doe";
	console.log(localVar);
	// console.log(globalVar); - accessible since the globalVar is a global variable
}
console.log(globalVar);

// console.log(localVar); - returns error because the localVar can only be accessed inside the curly brace where it is created

function showNames(){
	const functionConst = "Joe";
	let functionLet = "John";

	console.log(functionConst);
	console.log(functionLet);
}

showNames();

// Returns an error because they are inside the function showNames()
// console.log(functionConst);
// console.log(functionLet);

// Nested Functions
function myNewFunction(){
	let name = "Jane";

	function nestedFunction(){
		let nestedVar = "John";
		console.log(name); // Will work because nestedFunction() is still inside the function (myNewFunction) where "name" variable is declared
	}
	// console.log(nestedVar); - Results to an error because of function scoping in JS
	nestedFunction(); // Works because nestedFunction is called inside the function where it is declared
}
myNewFunction();
// nestedFunction(); - Doesn't work because nestedFunction is called outside the function where it is declared

let globalName = "Alexandro";
function NewFunction2(){
	let nameInside = "Renz";
	console.log(globalName);
}
NewFunction2();
// console.log(nameInside); - Results to an error

// [Section] use of alert()
// alert() allows us to show a small window at the top of our browser page to show information to our users
alert("Hello World");
// We can also use alert() to show a message to the users from a later function invocation
function sampleAlert(){
	alert("Hello, User!");
}
sampleAlert();

console.log('I will only log in the console after the alert is closed');

// [Section] use of prompt()
// prompt() allows us to show a small window at the top of the browser page to gather user input. Much like alert, it will have the page wait until the user completes or enters their input.
// Input from the prompt() will be returned as a String data type once the user dismisses the window
/*
prompt() returns empty string if the user clicks 'ok' button without entering any input and null if the user cancels the prompt()
*/
let samplePrompt = prompt("Enter your name:");
console.log("Hello, " + samplePrompt);
console.log(typeof samplePrompt); // Returns a String data type

/*
Mini activity
	- create a "printWelcomeMessage()" function that has the following specification:
		- create 2 variables that come from prompt():
			- firstName
			- lastName

		- log in the console the message "Hello firstName lastName!
		  Welcome to my page!"
*/

function printWelcomeMessage(){
	let firstName = prompt("First Name?:")
	let lastName = prompt("Last Name?:")
	console.log("Hello, " + firstName + ' ' + lastName + '!' + '\n' + "Welcome to my page!")
}
printWelcomeMessage();

// [Section] Function Naming Conversions
	// Function names should be definitive of the task it will perform. It usually starts with a verb.

	function getCourses(){
		let courses = [ "Science 101", "Math 101", "English 101" ];
		console.log(courses);
	}
	getCourses();

		// Avoid generic names to avoid confusion within our codes
	function get(){
		let name = "Jamie";
		console.log(name);
	}
	get();

		// Avoid pointless and inappropriate function names
	function foo(){
		console.log(25%5);
	}
	foo();

		// Name our functions in small caps. Follow cameCasing when naming variables and functions with more than 2 words

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1 500 000");
	}
	displayCarInfo();